﻿namespace CMS.Presentation.Models
{
    public class TransactionModel
    {
        public string utm_source { get; set; }
        public float transaction_value { get; set; }
        public string reason_rejected { get; set; }
        public string category_name { get; set; }
        public string utm_term { get; set; }
        public int product_id { get; set; }
        public bool is_confirmed { get; set; }
        public string confirmed_time { get; set; }
        public string product_image { get; set; }
        public string id { get; set; }
        public string commission { get; set; }
        public float product_price { get; set; }
        public int conversion_id { get; set; }
        public string utm_medium { get; set; }
        public int product_quantity { get; set; }
        public string click_time { get; set; }
        public string product_name { get; set; }
        public int transaction_id { get; set; }

        public Extra _extra { get; set; }
    }

    public class Extra
    {
        public string device_model { get; set; }
        public string device_family { get; set; }
        public string device_brand { get; set; }
        public string device_type { get; set; }
        public string device { get; set; }
        public string os { get; set; }
        public string browser { get; set; }

        public Parameters parameters { get; set; }
    }

    public class Parameters
    {
        public string utm_campaign { get; set; }
        public string click_url { get; set; }
        public string click_user_agent { get; set; }
        public string utm_tool { get; set; }
        public string utm_content { get; set; }
        public string utm_medium { get; set; }
        public string utm_source { get; set; }
    }
}