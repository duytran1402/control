﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Presentation.Models
{
    public class OrderModel
    {
        public string at_product_link { get; set; }
        public decimal billing { get; set; }
        public string browser { get; set; }
        public string category_name { get; set; }
        public DateTime click_time { get; set; }
        public string client_platform { get; set; }
        public DateTime confirmed_time { get; set; }
        public int is_confirmed { get; set; }
        public string landing_page { get; set; }
        public string merchant { get; set; }
        public string order_id { get; set; }
        public int order_pending { get; set; }
        public int order_reject { get; set; }
        public int order_success { get; set; }
        public string order_uid { get; set; }
        public string product_category { get; set; }
        public List<Product> products { get; set; }
        public int products_count { get; set; }
        public decimal pub_commission { get; set; }
        public DateTime sales_time { get; set; }
        public int total_commission { get; set; }
        public string utm_campaign { get; set; }
        public string utm_medium { get; set; }
        public string utm_source { get; set; }
        public string website { get; set; }
        public string website_url { get; set; }
    }

    public class At
    {
        public int banner_id { get; set; }
        public int commission_type { get; set; }
        public string goods_id { get; set; }
        public int result_id { get; set; }
        public int reward_type { get; set; }
        public int seq_no { get; set; }
    }

    public class ParameterOrders
    {
        public string click_url { get; set; }
        public string click_user_agent { get; set; }
        public string utm_tool { get; set; }
        public string utm_campaign { get; set; }
        public string utm_content { get; set; }
        public string utm_medium { get; set; }
        public string utm_source { get; set; }
        public string click_referer { get; set; }
    }

    public class ExtraOrder
    {
        public string browser { get; set; }
        public string device { get; set; }
        public object device_brand { get; set; }
        public string device_family { get; set; }
        public object device_model { get; set; }
        public string device_type { get; set; }
        public string os { get; set; }
        public ParameterOrders parameters { get; set; }
    }

    public class Product
    {
        public At _at { get; set; }
        public ExtraOrder _extra { get; set; }
        public int amount { get; set; }
        public string campaign_id { get; set; }
        public DateTime click_time { get; set; }
        public DateTime confirmed_time { get; set; }
        public string merchant { get; set; }
        public string product_id { get; set; }
        public decimal product_price { get; set; }
        public int product_quantity { get; set; }
        public decimal pub_commission { get; set; }
        public string reason_rejected { get; set; }
        public DateTime sales_time { get; set; }
        public int status { get; set; }
    }
}