﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CMS.Presentation.Models
{
    public class HistoryModel
    {
        public string Merchant { get; set; }
        //[Required]
        public string UserId { get; set; }
        //[Required]
        public int TransactionId { get; set; }
    }
}