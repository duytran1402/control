﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Presentation.Helpers
{
    public class Constant
    {
        public static readonly string ACCESSTRADE = "accesstrade";
        public static readonly string ECOMOBI = "ecomobi";
    }
}