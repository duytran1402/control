namespace CMS.Infra.Data.Context
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init1 : DbMigration
    {
        public override void Up()
        {
            MoveTable(name: "Captcha.Category", newSchema: "CMS");
        }
        
        public override void Down()
        {
            MoveTable(name: "CMS.Category", newSchema: "Captcha");
        }
    }
}
